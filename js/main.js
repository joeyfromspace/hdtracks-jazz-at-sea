// Custom functions for Blue Note Landing Page

(function() {
	var fixEntryBox = function() {
		if(window.ifMobile()) {
			$('.container-enter').css({marginTop: 0, backgroundColor: 'transparent', backgroundImage: "none"});
			$('.artists').css({marginTop: 30+'px', position: 'relative', bottom: '0'});
			$('footer').css({position: 'relative'});
		} else {
		    if(($('.artists').offset().top + parseInt($('.artists').height())) > ($('.page-copy').offset().top)) {
			    $('.page-copy').css('margin-top', Math.floor(parseInt($('.artists').height()) * 0.4));
		    }
			$('.container-enter').valign(Math.floor(parseInt($('.artists').height()) * 0.20) + 70).css({backgroundColor: '#006699'});
			$('.artists').attr('style', '');
			$('footer').css({position: 'absolute'});
		}		
		$('.background').stretch('body');
	}
	$(document).ready(function() {
		if(getQueryVariable('success')) {						
			$('.reveal-success').css({ display: 'block' }).velocity({ opacity: [1,0], translateY: [0, -50]}, { stagger: 200, delay: 1000, duration: 200, easing: 'ease-in'});			
		} else if(getQueryVariable('failure')) {
			$('.reveal-failure').css({ display: 'block' }).velocity({ opacity: [1,0], translateY: [0, -50]}, { stagger: 200, delay: 1000, duration: 200, easing: 'ease-in'});			
		} else {			
			var busy = false;
			var i = 0;
			$('.reveal-enter').each(function() {				
				var $this = $(this);				
				var animInterval = setInterval(function() {					
					if(!busy) {
						i++;
						var delay = i == 1 ? 1000 : 200;
						busy = true;
						clearInterval(animInterval);						
						$this.css({display: 'block'});
						var ty = $this.hasClass('container-enter') ? 1 : 0;
						$this.velocity({ opacity: [1,0], translateY: [0,-50] }, { delay: delay, duration: 200, easing: 'ease-in', complete: function() {
							busy = false;
							if(ty && window.ifMobile() === false) {
								$this.valign().velocity({ marginTop: function() {
									return parseInt($this.css('margin-top')) + Math.floor(parseInt($('.artists').height()) * 0.20);
								}}, { duration: 200, easing: 'ease-in' , complete:function() {
									$('.page-copy').velocity({ marginTop: function() {										
										return parseInt($('.page-copy').css('margin-top')) + Math.floor(parseInt($('.artists').height()) * 0.20);
									}},{ duration: 200, easing: 'ease-in', complete:function() {
										fixEntryBox();
									}});
								}});
							} else {
								fixEntryBox();
							}
						}});						
					}
				}, 10);
			});
		}
		fixEntryBox();
		$(window).resize(function() {
			fixEntryBox();
		});		
	});
})();

// Utility functions
(function() {
	window.randomRange = function(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	};
	window.getQueryVariable = function(variable) {
	    var query = window.location.search.substring(1);
	    var vars = query.split("&");
	    for(var i = 0; i < vars.length; i++) {
	        var pair = vars[i].split("=");
	        if(pair[0] == variable) {
	            return pair[1] || pair[0];
	        }
	    }
	    return false;
	};
	window.ifMobile = function() {
		return $(window).width() <= 768 ? true : false;
	};
})();

// Custom jQuery methods
$.fn.extend({
	valign: function(offset) {
		if($(this).height() < $(window).height()) {
			var padding = (($(window).height() - $(this).height()) / 2) + (offset || 0);
			$(this).css({marginTop: padding});
		}
		return this;
	}, stretch: function(selector) {
		// Requires selector to stretch to
		if(typeof(selector) == 'undefined') {
			return false;
		}

		if($(this).height() < $(selector).prop('scrollHeight')) {
			$(this).css({height: $(selector).prop('scrollHeight')});
		}
		return this;
	}, observeHeight: function(callback) {
		var _this = this;
		var oh = $(this).prop('scrollHeight');
		var observer = setInterval(function() {
			var ch = $(_this).prop('scrollHeight');
			if(ch != oh) {
				callback(oh, ch);
				oh == ch;
			}
		},200);
	}
});